resource "aws_db_instance" "rdsinstance" {
  allocated_storage    = 10
  storage_type         = "gp2"
  engine               = "${var.myengine}"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "root"
  password             = "dsfkljsfkljsdklf"
  parameter_group_name = "default.mysql5.7"
}